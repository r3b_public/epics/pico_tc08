/*
 * pico_tc08.h
 *
 * Asyn driver that inherits from the asynPortDriver class to support the
 * Picotech TC08 USB thermometer.
 *
 * Author: Bastian Löher
 *
 * Created Sep. 26, 2018
 */

#include "asynPortDriver.h"

#include <usbtc08.h>

#define N_CH 8

struct UsbTc08Channel
{
	int8_t tc_type;
};

struct UsbTc08Config
{
	UsbTc08Channel ch[N_CH];
};

struct UsbTc08Module
{
	int16_t handle;
	struct UsbTc08Config config;
	int32_t read_interval_ms;
};

enum Tc08NoiseReject
{
	USBTC08_NR_50Hz,
	USBTC08_NR_60Hz
};

enum Tc08TcTypes
{
	USBTC08_TC_B,
	USBTC08_TC_E,
	USBTC08_TC_J,
	USBTC08_TC_K,
	USBTC08_TC_N,
	USBTC08_TC_R,
	USBTC08_TC_S,
	USBTC08_TC_T,
	USBTC08_TC_VOLTAGE,
	USBTC08_TC_DISABLE,
	USBTC08_TC_MAX
};

/*
 * Class that uses asynPortDriver base class to access the TC08 USB
 * thermometer from Picotech
 */
class pico_tc08 : public asynPortDriver {
public:
    pico_tc08(const char *portName);

    /* These are the methods that we override from asynPortDriver */
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);

    /* These are the methods that are new to this class */
    void run_task(void);

protected:
    /** Values used for pasynUser->reason, and indexes into the parameter library. */
    int P_Run = 0;

    int P_PicoStatus = 0;
    int P_PicoConnect = 0;
    int P_PicoConnected = 0;
    int P_ReadIntervalMs = 0;
    int P_junction_temperature = 0;
    int P_Units = 0;
    int P_ch_tc_type[N_CH] = {0};
    int P_ch_tc_type_select[N_CH] = {0};
    int P_ch_temperature[N_CH] = {0};

private:
    /* Our data */
    epicsEventId eventId_;
    float in_buffer[N_CH + 1];

    void connectTc08(void);
    int closeTc08(void);
    int openTc08(void);
    int set_channel(int);
    int run_block(void);
    void print_unit_info(void);

    /* pico data members */
    struct UsbTc08Module ps;
};

#define CHKOK_OR_RETURN(name) do {\
	setIntegerParam(P_PicoStatus, ok);\
	if (ok != USBTC08_ERROR_OK) {\
		printf("ERROR %d in %s: %s\n", ok, name, tc08_strings[ok]);\
		return ok;\
	}\
}while (0)
#define CHKOK(name) do {\
	setIntegerParam(P_PicoStatus, ok);\
	if (ok != USBTC08_ERROR_OK) {\
		printf("ERROR %d in %s: %s\n", ok, name, tc08_strings[ok]);\
	}\
}while (0)

