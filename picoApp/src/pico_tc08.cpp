/*
 * pico_tc08.cpp
 *
 * Asyn driver that inherits from the asynPortDriver class to support the
 * Picotech TC08 USB thermometer.
 *
 * Author: Bastian Löher
 *
 * Created Sep. 26, 2018
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>

#include <epicsExport.h>
#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <iocsh.h>

#include "pico_tc08.h"

static const char *driverName="pico_tc08";
void run_task(void *drvPvt);

static const int8_t tc_types[] =
{
	'B',
	'E',
	'J',
	'K',
	'N',
	'R',
	'S',
	'T',
	'X',
	' ',
	0
};

#if 0
static const char *tc08_info_strings[] =
{
	"driver version",
	"kernel driver version",
	"hardware version",
	"variant info",
	"batch and serial",
	"calibration date",
	"driver path",
	NULL
};
#endif

static const char *tc08_strings[] =
{
	"OK",
	"OS not supported",
	"No channels set",
	"Invalid parameter",
	"Variant not supported",
	"Incorrect mode",
	"Enumeration incomplete",
	"Not responding",
	"Fw fail",
	"Config fail",
	"Not found",
	"Thread fail",
	"Pipe info fail",
	"Not calibrated",
	"Picopp too old",
	"Pico driver function",
	"Communication",
	NULL
};


/*
 * Constructor for the pico_tc08 class.
 * Calls constructor for the asynPortDriver base class.
 * \param[in] portName The name of the asyn port driver to be created.
 */
pico_tc08::pico_tc08(const char *portName)
   : asynPortDriver(portName,
    1, /* maxAddr */
    asynInt32Mask | asynFloat64Mask
    | asynDrvUserMask, /* Interface mask */
    asynInt32Mask | asynFloat64Mask,  /* Interrupt mask */
    0,
    1, /* Autoconnect */
    0, /* Default priority */
    0) /* Default stack size*/
{
	int ch;
	asynStatus status;
	const char *functionName = "pico_tc08";

	eventId_ = epicsEventCreate(epicsEventEmpty);

	/* Create parameters */
	createParam("run", asynParamInt32, &P_Run);
	createParam("read_interval_ms", asynParamInt32, &P_ReadIntervalMs);
	createParam("units", asynParamInt32, &P_Units);
	createParam("junction_temperature", asynParamFloat64,
	    &P_junction_temperature);
	createParam("PICO_STATUS", asynParamInt32, &P_PicoStatus);
	createParam("PICO_CONNECT", asynParamInt32, &P_PicoConnect);
	createParam("PICO_CONNECTED", asynParamInt32, &P_PicoConnected);

	for (ch = 0; ch < N_CH; ++ch) {
		char buf[BUFSIZ];

		sprintf(buf, "ch_%d_tc_type", ch + 1);
		createParam(buf, asynParamInt32, &P_ch_tc_type[ch]);

		sprintf(buf, "ch_%d_tc_type_select", ch + 1);
		createParam(buf, asynParamInt32, &P_ch_tc_type_select[ch]);

		sprintf(buf, "ch_%d_temperature", ch + 1);
		createParam(buf, asynParamFloat64, &P_ch_temperature[ch]);
	}

	/* Initialise parameter values */
	for (ch = 0; ch < N_CH; ++ch) {
		setDoubleParam(P_ch_temperature[ch], 0);
		setIntegerParam(P_ch_tc_type[ch], 'K');
		setIntegerParam(P_ch_tc_type_select[ch], USBTC08_TC_K);
	}

	for (ch = 0; ch < N_CH + 1; ++ch) {
		in_buffer[ch] = 0;
	}

	setIntegerParam(P_Run, 0);
	setIntegerParam(P_Units, USBTC08_UNITS_CENTIGRADE);
	setIntegerParam(P_PicoStatus, 0);
	setIntegerParam(P_PicoConnected, 0);
	setIntegerParam(P_PicoConnect, 0);
	setIntegerParam(P_ReadIntervalMs, 1000);
	setDoubleParam(P_junction_temperature, 0);

	/* Create the thread that runs the waveforms acq in the background */
	status = (asynStatus)(epicsThreadCreate("pico_tc08_task",
		    epicsThreadPriorityMedium,
		    epicsThreadGetStackSize(epicsThreadStackMedium),
		    (EPICSTHREADFUNC)::run_task, this) == NULL);

	if (status) {
		printf("%s:%s: epicsThreadCreate failure\n", driverName,
		    functionName);
		return;
	}

}

void
run_task(void *drvPvt)
{
    pico_tc08 *pPvt = (pico_tc08 *)drvPvt;
    pPvt->run_task();
}

void
pico_tc08::run_task()
{
	epicsInt32 run;
	epicsInt32 read_interval_ms;

	lock();

	while(1)
	{
		getIntegerParam(P_ReadIntervalMs, &read_interval_ms);
		getIntegerParam(P_Run, &run);

		unlock();

		float update_time_s = (float)read_interval_ms / 1000.;

		if (run) epicsEventWaitWithTimeout(eventId_, update_time_s);
		else     (void) epicsEventWait(eventId_);

		lock();

        	getIntegerParam(P_Run, &run);
        	if (!run) continue;

		run_block();
	}
}

int
pico_tc08::run_block()
{
	enUSBTC08Error ok;
	epicsInt32 units;
	int ch;

	getIntegerParam(P_Units, &units);

	ok = (enUSBTC08Error)usb_tc08_get_single(ps.handle, in_buffer, NULL,
	    (enum enUSBTC08Units)units);
	/* TODO: This does not work CHKOK("RunBlock"); */
	(void)ok;

	setDoubleParam(P_junction_temperature, in_buffer[0]);
	for (ch = 0; ch < N_CH; ++ch) {
		setDoubleParam(P_ch_temperature[ch], in_buffer[ch + 1]);
	}

        callParamCallbacks();

	return 0;
}

/** Called when asyn clients call pasynInt32->write().
  * This function sends a signal to the simTask thread if the value of P_Run has changed.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus pico_tc08::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
	int ch;
	int function = pasynUser->reason;
	asynStatus status = asynSuccess;
	const char *paramName;
	const char* functionName = "writeInt32";

	/* Set the parameter in the parameter library. */
	status = (asynStatus) setIntegerParam(function, value);

	/* Fetch the parameter string name for possible use in debugging */
	getParamName(function, &paramName);

	for (ch = 0; ch < N_CH; ++ch) {
		if (function == P_ch_tc_type_select[ch]) {
			epicsInt32 type = USBTC08_TC_DISABLE;

			printf("value = %d\n", value);
			switch (value)
			{
			case USBTC08_TC_B: type = 'B'; break;
			case USBTC08_TC_E: type = 'E'; break;
			case USBTC08_TC_J: type = 'J'; break;
			case USBTC08_TC_K: type = 'K'; break;
			case USBTC08_TC_N: type = 'N'; break;
			case USBTC08_TC_R: type = 'R'; break;
			case USBTC08_TC_S: type = 'S'; break;
			case USBTC08_TC_T: type = 'T'; break;
			case USBTC08_TC_VOLTAGE: type = 'X'; break;
			case USBTC08_TC_DISABLE: type = ' '; break;

			default: break;
			}

			printf("type = '%c'\n", type);
			setIntegerParam(P_ch_tc_type[ch], type);
			ps.config.ch[ch].tc_type = type;

			set_channel(ch);
		}
	}

	if (function == P_Run) {
		/* If run was set then wake up the simulation task */
		if (value) epicsEventSignal(eventId_);
	}
	else if (function == P_PicoConnect) {
		connectTc08();
	}
	else {
		/* All other parameters just get set in parameter list, no need to
		 * act on them here */
	}

	/* Do callbacks so higher layers see any changes */
	status = (asynStatus) callParamCallbacks();

	if (status) {
		epicsSnprintf(pasynUser->errorMessage,
		    pasynUser->errorMessageSize, "%s:%s: status=%d, "
		    "function=%d, name=%s, value=%d", driverName, functionName,
		    status, function, paramName, value);
	} else {
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
		    "%s:%s: function=%d, name=%s, value=%d\n",
		    driverName, functionName, function, paramName, value);
	}
	return status;
}

/** Called when asyn clients call pasynFloat64->write().
  * This function sends a signal to the simTask thread if the value of P_UpdateTime has changed.
  * For all  parameters it  sets the value in the parameter library and calls any registered callbacks.
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus pico_tc08::writeFloat64(asynUser *pasynUser, epicsFloat64 value)
{
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    const char *paramName;
    const char* functionName = "writeFloat64";

    /* Set the parameter in the parameter library. */
    status = (asynStatus) setDoubleParam(function, value);

    /* Fetch the parameter string name for possible use in debugging */
    getParamName(function, &paramName);

    /* Do callbacks so higher layers see any changes */
    status = (asynStatus) callParamCallbacks();

    if (status)
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
                  "%s:%s: status=%d, function=%d, name=%s, value=%f",
                  driverName, functionName, status, function, paramName, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:%s: function=%d, name=%s, value=%f\n",
              driverName, functionName, function, paramName, value);
    return status;
}

int
pico_tc08::openTc08()
{
	int ret;
	enUSBTC08Progress progress;

	ps.handle = -1;
	ret = usb_tc08_open_unit_async();
	if (ret == 0) {
		enUSBTC08Error ok = USBTC08_ERROR_NOT_FOUND;
		CHKOK_OR_RETURN("open_unit_async");
	}
	if (ret == -1) {
		enUSBTC08Error ok;
		ok = (enUSBTC08Error)usb_tc08_get_last_error(0);
		CHKOK_OR_RETURN("open_unit_async");
	}

	printf("open_unit ");
	while((progress = (enUSBTC08Progress)
		    usb_tc08_open_unit_progress(&ps.handle, NULL))
	    == USBTC08_PROGRESS_PENDING)
	{
		printf("|");
		fflush(stdout);
		epicsThreadSleep(200e-3);
	}
	puts("");

	if (progress != USBTC08_PROGRESS_COMPLETE || ps.handle < 0) {
		printf("ERROR open_unit_async: %d\n", progress);
		return ret;
	}
	printf("open_unit succeeded, handle = %d\n", ps.handle);

	ret = usb_tc08_set_mains(ps.handle, USBTC08_NR_50Hz);
	if (ret == 0) {
		enUSBTC08Error ok;
		ok = (enUSBTC08Error)usb_tc08_get_last_error(ps.handle);
		CHKOK("set_mains");
	}

	setIntegerParam(P_PicoConnected, 1);

	return 0;
}

int
pico_tc08::closeTc08()
{
	enUSBTC08Error ok;
	ok = (enUSBTC08Error)usb_tc08_close_unit(ps.handle);
	(void)ok;
	/* TODO: Does not work CHKOK("Close"); */
	setIntegerParam(P_PicoConnected, 0);

	return 0;
}

/* This method will also enable the channels. By default disabled. */
int
pico_tc08::set_channel(int ch)
{
	int ok;
	epicsInt32 connected;
	int channel = USBTC08_CHANNEL_1 + ch;

	epicsInt32 tc_type;

	getIntegerParam(P_ch_tc_type[ch], &tc_type);
	getIntegerParam(P_PicoConnected, &connected);

	printf("Set channel %d: type = %c\n", ch, (char)tc_type);

	if (connected != 0) {
		ok = usb_tc08_set_channel(ps.handle, channel, tc_type);
		/* TODO: This does not work
		 * CHKOK("SetChannel");
		 */
		(void)ok;
	}

	return 0;
}

void
pico_tc08::print_unit_info()
{
	USBTC08_INFO info;

	printf("------- Unit info -------\n");
	info.size = sizeof(USBTC08_INFO);
	usb_tc08_get_unit_info(ps.handle, &info);
	printf("driver version: %s\n", info.DriverVersion);
	printf("picopp version: %d\n", info.PicoppVersion);
	printf("hardware version: %d\n", info.HardwareVersion);
	printf("hardware variant: %d\n", info.Variant);
	printf("serial number: %s\n", info.szSerial);
	printf("calibration date: %s\n", info.szCalDate);
	printf("----- End Unit info -----\n");
}


void
pico_tc08::connectTc08()
{
	epicsInt32 connect, connected;
	getIntegerParam(P_PicoConnect, &connect);
	getIntegerParam(P_PicoConnected, &connected);

	printf("PICO connectTc08 connect = %d\n", connect);

	if (connect == 0 && connected == 1) {
		printf("PICO Disconnecting...\n");
		closeTc08();
		setIntegerParam(P_PicoConnected, 0);
	} else if (connected == 0 && connect == 1) {
		int ch;
		int ret;

		printf("PICO Connecting...\n");
		if(openTc08() != 0) {
			return;
		}
		for (ch = 0; ch < N_CH; ch++) {
			set_channel(ch);
		}

		ret = usb_tc08_get_minimum_interval_ms(ps.handle);
		if (ret == 0) {
			enUSBTC08Error ok;
			ok = (enUSBTC08Error)usb_tc08_get_last_error(ps.handle);
			CHKOK("get_minimum_interval_ms");
		}

		printf("minimum interval = %d ms\n", ret);

		print_unit_info();
	}
}

/* Configuration routine.  Called directly, or from the iocsh function below */

extern "C" {

/*
 * EPICS iocsh callable function to call constructor for the pico_tc08 class.
 * \param[in] portName The name of the asyn port driver to be created.
 */
int pico_tc08Configure(const char *portName)
{
    new pico_tc08(portName);
    return(asynSuccess);
}

/* EPICS iocsh shell commands */

static const iocshArg initArg0 = { "portName",iocshArgString};
static const iocshArg * const initArgs[] = {&initArg0};
static const iocshFuncDef initFuncDef = {"pico_tc08Configure",1,initArgs};

static void
initCallFunc(const iocshArgBuf *args)
{
	pico_tc08Configure(args[0].sval);
}

void
pico_tc08Register(void)
{
	iocshRegister(&initFuncDef,initCallFunc);
}

epicsExportRegistrar(pico_tc08Register);

}

