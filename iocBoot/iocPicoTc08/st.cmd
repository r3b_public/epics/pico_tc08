#!../../bin/linux-x86_64/pico_tc08

dbLoadDatabase("../../dbd/pico_tc08.dbd")
pico_tc08_registerRecordDeviceDriver(pdbbase)

# Turn on asynTraceFlow and asynTraceError for global trace,
# i.e. no connected asynUser.
#asynSetTraceMask("", 0, 17)
#asynSetTraceMask("temp",0,0xff)
#asynSetTraceIOMask("temp",0,0x2)

pico_tc08Configure("temp", 1000)

dbLoadRecords("../../db/pico_tc08.db","P=temp:,R=1:,PORT=temp,ADDR=0,TIMEOUT=1")
dbLoadRecords("../../db/pico_tc08_ch.db","P=temp:,R=1:,PORT=temp,ADDR=0,TIMEOUT=1")
#dbLoadRecords("../../db/asynRecord.db","P=temp:,R=asyn1,PORT=temp,ADDR=0,OMAX=80,IMAX=80")

iocInit()
